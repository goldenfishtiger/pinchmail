#!/usr/bin/env ruby
# coding: utf-8

require 'gdbm'
require 'net/pop'
require 'openssl'

load 'mave.config'

#===============================================================================
#
#	メールアカウントモデルクラス
#
class MaveAccount

	attr_reader :name
	attr_reader :pop_server

	def initialize(params)
		@configs		= params[:CONFIGS]
		@account		= params[:ACCOUNT]

		@name			= @account[:NAME]

		@pop_server		= @account[:POP_SERVER]
		@pop_port		= @account[:POP_PORT]
		@pop_account	= @account[:POP_ACCOUNT]
		@pop_password	= @account[:POP_PASSWORD]
		@pop_over_ssl	= @account[:POP_OVER_SSL]
		@pop_ssl_verify	= @account[:POP_SSL_VERIFY]
		@pop_ssl_certs	= @account[:POP_SSL_CERTS]
		@pop_keep_time	= @account[:POP_KEEP_TIME]

		@pop_uids		=     GDBM.new(@configs[:ROOT_DIRECTORY] + "/pop_uids_#{name}", 0600)
	end

	#-----------------------------------------------------------
	#
	#	POP する
	#
	def pop
		now = Time.new.to_i
		begin
			pop = Net::POP3.new(@pop_server, @pop_port)
			pop.enable_ssl(@pop_ssl_verify, @pop_ssl_certs) if(@pop_over_ssl)
			pop.start(@pop_account, @pop_password) {|pop|
				yield('Connected.')
				popmails = []
				pop.mails.each {|popmail|
					if(it = @pop_uids[popmail.unique_id])		# 既知のメール
						if(it.to_i < now - @pop_keep_time)
							popmail.delete
							@pop_uids.delete(popmail.unique_id)
						end
					else										# 未知のメール
						popmails << popmail
					end
				}
				yield(popmails.size)
				popmails.each {|popmail|
					yield(popmail)
					@pop_uids[popmail.unique_id] = now.to_s
				}
				@pop_uids.keys.each {|unique_id|
					@pop_uids.delete(unique_id) if(@pop_uids[unique_id].to_i < now - @pop_keep_time)
				}
				@pop_uids.reorganize
			} if(@pop_server)
		rescue StandardError, Timeout::Error
			raise($!.message.split(/\r?\n/)[0])
		end
	end
end

#---------------------------------------------------------------
#
#	メール受信(POP)
#
account = MaveAccount.new({:CONFIGS => @configs, :ACCOUNT => @configs[:ACCOUNTS][0]})
begin
	account.pop {|popmail|
		next if(popmail.is_a?(String))
		next if(popmail.is_a?(Integer))
		IO.popen(['/usr/bin/procmail', './procmailrc'], 'w') {|procmail|
			popmail.pop {|line|
				procmail.write(line)
			}
		}
	}
rescue
	puts('Failed to pop mail. reason=[%s]' % $!.message.split(/\r?\n/)[0])
end

__END__

