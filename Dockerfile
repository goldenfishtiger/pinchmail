FROM fedora:32

LABEL maintainer="Furutanian <furutanian@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		postfix \
		dovecot \
		procmail \
		cronie \
		ruby \
		passwd \
		procps-ng \
		iputils \
		net-tools \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone pinchmail しておくこと
COPY pinchmail /root/pinchmail

ENV target=/etc/postfix/main.cf
RUN set -x \
	&& mv $target $target.org && cat $target.org \
		| sed 's/^\(inet_interfaces \).*/\1= all/' \
		> $target && diff -C 2 $target.org $target || echo "$target changed."

ENV target=/etc/dovecot/conf.d/10-mail.conf
RUN set -x \
	&& mv $target $target.org && cat $target.org \
		| sed 's/^#\(mail_location \).*/\1= mbox:~\/mail:INBOX=\/var\/mail\/%u/' \
		> $target && diff -C 2 $target.org $target || echo "$target changed."

ENV target=/etc/dovecot/conf.d/10-auth.conf
RUN set -x \
	&& mv $target $target.org && cat $target.org \
		| sed 's/^#\(disable_plaintext_auth \).*/\1= no/' \
		> $target && diff -C 2 $target.org $target || echo "$target changed."

ENV target=/etc/dovecot/conf.d/10-ssl.conf
RUN set -x \
	&& mv $target $target.org && cat $target.org \
		| sed 's/^\(ssl \).*/\1= no/' \
		> $target && diff -C 2 $target.org $target || echo "$target changed."

RUN systemctl enable postfix
RUN systemctl enable dovecot
RUN systemctl enable crond

EXPOSE 25 110

WORKDIR /root/pinchmail

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
cat Dockerfile | sed -n \'/^##__BEGIN1/,/^##__END1/p\' | sed \'s/^#//\' > crontab.index\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	echo 'startup.sh start.'
#
#	if [ ! -e /var/lib/pinchmail/pv/mave.config ]; then
#		cp -v mave.config.sample procmailrc.sample /var/lib/pinchmail/pv
#		echo 'Rename 'pv/mave.config.sample' to 'pv/mave.config' and modify it.'
#		echo 'Rename 'pv/procmailrc.sample' to 'pv/procmailrc' and modify it.'
#		tail -f /dev/null
#	fi
#
#	cp -v /var/lib/pinchmail/pv/dot.bashrc /home/user/.bashrc
#	cp -v /var/lib/pinchmail/pv/dot.virc /home/user/.virc
#	cp -v /var/lib/pinchmail/pv/dot.gitconfig /home/user/.gitconfig
#
#	for user in $mail_users; do
#		useradd -m $user
#		echo "$password_prefix$user" | passwd --stdin $user
#		echo "user [$user] added."
#	done
#
#	if [ ! -e /var/lib/pinchmail/pv/spool_postfix ]; then
#		cp -av /var/spool/postfix /var/lib/pinchmail/pv/spool_postfix
#		cp -av /var/lib/postfix   /var/lib/pinchmail/pv/lib_postfix
#		cp -av /var/spool/mail    /var/lib/pinchmail/pv/spool_mail
#	fi
#	rm -rfv /var/spool/postfix
#	rm -rfv /var/lib/postfix
#	rm -rfv /var/spool/mail
#	ln -sv /var/lib/pinchmail/pv/spool_postfix /var/spool/postfix
#	ln -sv /var/lib/pinchmail/pv/lib_postfix   /var/lib/postfix
#	ln -sv /var/lib/pinchmail/pv/spool_mail    /var/spool/mail
#
#	chmod 600 /var/spool/mail/*
#
#	ln -sv /var/lib/pinchmail/pv/mave.config /root/pinchmail
#	ln -sv /var/lib/pinchmail/pv/procmailrc  /root/pinchmail
#
#	crontab crontab.index
#	crontab -l
#
#	echo 'startup.sh done.'
#
##__END0__startup.sh__

##__BEGIN1__crontab.index__
#
#	MAILTO=""
#
#	* * * * * cd /root/pinchmail; ./pinchmail
#
##__END1__crontab.index__

